package com.prueba.cristian.prueba.repositories;

import com.prueba.cristian.prueba.models.LocationModel;
import org.springframework.data.repository.CrudRepository;

import java.util.ArrayList;

public interface LocationRepository  extends CrudRepository<LocationModel, Long> {

    public abstract ArrayList<LocationModel> findByType(String type);

    public abstract Integer countByType(String type);
}
