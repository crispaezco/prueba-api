package com.prueba.cristian.prueba.repositories;

import com.prueba.cristian.prueba.models.ManagerModel;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ManagerRepository extends CrudRepository<ManagerModel, String> {
}
