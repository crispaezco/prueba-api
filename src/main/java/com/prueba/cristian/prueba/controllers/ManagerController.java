package com.prueba.cristian.prueba.controllers;

import com.prueba.cristian.prueba.models.ManagerModel;
import com.prueba.cristian.prueba.services.ManagerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;

@RestController
@CrossOrigin(origins = "*", methods= {RequestMethod.GET,RequestMethod.POST})
@RequestMapping("/gestores")
public class ManagerController {
    @Autowired
    ManagerService managerService;

    @PostMapping()
    public ArrayList<ManagerModel> getManagers() {
        return managerService.getManagers();
    }
}
