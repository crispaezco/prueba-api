package com.prueba.cristian.prueba.controllers;

import com.prueba.cristian.prueba.models.LocationModel;
import com.prueba.cristian.prueba.services.LocationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

@RestController
@CrossOrigin(origins = "*", methods= {RequestMethod.GET,RequestMethod.POST})
@RequestMapping("/localidades")
public class LocationController {
    @Autowired
    LocationService locationService;

    @GetMapping()
    public ArrayList<LocationModel> getLocations() {
        return locationService.getLocations();
    }

    @PostMapping("/ciudades")

    public Map<String, Object> getCities() {
        HashMap<String, Object> map = new HashMap<>();
        map.put("count", locationService.countCities());
        map.put("cities", locationService.getCities());
        return map;
    }
}
