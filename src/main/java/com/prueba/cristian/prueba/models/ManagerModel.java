package com.prueba.cristian.prueba.models;

import org.hibernate.annotations.Formula;

import javax.persistence.*;

@Entity
@Table(name = "COB_GESTOR", schema = "GCCOBRANZAS")
public class ManagerModel {

    @Id
    @Column(name = "USUARIO_ID")
    private String id;

    @Column(name = "PRIMER_NOMBRE")
    private String firstName;

    @Column(name = "SEGUNDO_NOMBRE")
    private String secondName;

    @Column(name = "PRIMER_APELLIDO")
    private String surname;

    @Column(name = "SEGUNDO_APELLIDO")
    private String secondSurname;

    @Formula(value = "REPLACE(concat(PRIMER_NOMBRE, ' ', SEGUNDO_NOMBRE, ' ', PRIMER_APELLIDO, ' ', SEGUNDO_APELLIDO), '  ', ' ')")
    private String fullName;


    public String getId() {
        return this.id;
    }

    public String getFirstName() {
        return this.firstName;
    }

    public String getSecondName() {
        return this.secondName;
    }

    public String getSurname() {
        return this.surname;
    }

    public String getSecondSurname() {
        return this.secondSurname;
    }

    public String getFullName() {
        return this.fullName;
    }
}
