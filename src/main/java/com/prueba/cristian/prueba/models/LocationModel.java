package com.prueba.cristian.prueba.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "GEN_LOCALIDAD", schema = "GCINTEGRIDAD")
public class LocationModel {
    @Id
    @Column(name = "LOCALIDAD_ID")
    private Long id;

    @Column(name = "NOMBRE")
    private String name;

    @Column(name = "TIPO")
    private String type;

    public Long getId() {
        return this.id;
    }

    public String getName() {
        return this.name;
    }

    public String getType() {
        return this.type;
    }
}
