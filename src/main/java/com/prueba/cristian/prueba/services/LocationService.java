package com.prueba.cristian.prueba.services;

import com.prueba.cristian.prueba.models.LocationModel;
import com.prueba.cristian.prueba.repositories.LocationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

@Service
public class LocationService {
    @Autowired
    LocationRepository locationRepository;

    public ArrayList<LocationModel> getLocations() {
        return (ArrayList<LocationModel>) locationRepository.findAll();
    }

    public ArrayList<LocationModel> getCities() {
        return (ArrayList<LocationModel>) locationRepository.findByType("CIUDAD");
    }

    public Integer countCities() {
        return locationRepository.countByType("CIUDAD");
    }
}
