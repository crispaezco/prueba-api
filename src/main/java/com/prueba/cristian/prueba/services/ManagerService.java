package com.prueba.cristian.prueba.services;

import com.prueba.cristian.prueba.models.ManagerModel;
import com.prueba.cristian.prueba.repositories.ManagerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

@Service
public class ManagerService {
    @Autowired
    ManagerRepository managerRepository;

    public ArrayList<ManagerModel> getManagers(){
        return (ArrayList<ManagerModel>) managerRepository.findAll();
    }
}
